import React, { createContext, useReducer, useContext } from 'react'

var NewsStateContext = createContext()
var NewsDispatchContext = createContext()

function newsReducer (state, action) {
  switch (action.type) {
    case 'SET_NEWS_DATA':
      return {
        ...state,
        newsData: action.newsData.map((item, idx) => {
          return {
            ...item,
            id: idx,
            isFavorite: false
          }
        })
      }
    case 'TOGGLE_FAVORITE':
      const { newsData } = state
      newsData.splice(action.itemId, 1, { ...newsData[action.itemId], isFavorite: !newsData[action.itemId].isFavorite })

      return {
        ...state,
        newsData
      }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

function NewsProvider ({ children }) {
  var [state, dispatch] = useReducer(newsReducer, { newsData: [] })

  return (
    <NewsStateContext.Provider value={state}>
      <NewsDispatchContext.Provider value={dispatch}>
        {children}
      </NewsDispatchContext.Provider>
    </NewsStateContext.Provider>
  )
}

function useNewsState () {
  var context = useContext(NewsStateContext)
  if (context === undefined) {
    throw new Error('useNewsState must be used within a NewsProvider')
  }
  return context
}

function useNewsDispatch () {
  var context = useContext(NewsDispatchContext)
  if (context === undefined) {
    throw new Error('useNewsDispatch must be used within a NewsProvider')
  }
  return context
}

function setNewsData (dispatch, newsData) {
  dispatch({ type: 'SET_NEWS_DATA', newsData })
}

function toggleFavorite (dispatch, itemId) {
  dispatch({ type: 'TOGGLE_FAVORITE', itemId })
}

export { NewsProvider, useNewsState, useNewsDispatch, setNewsData, toggleFavorite }
