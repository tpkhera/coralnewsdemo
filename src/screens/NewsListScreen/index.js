import React, { useEffect, useState } from 'react'
import {
  View,
  Text,
  StatusBar,
  ActivityIndicator,
  Image,
  FlatList,
  TouchableOpacity
} from 'react-native'

import { fetchNews } from '@@helpers/request-helper'
import { SCREEN_NAME_NEWSDETAILS } from '@@helpers/constants-helper'
import { getTimePassedSince } from '@@helpers/utility-helper'
import { useNewsState, useNewsDispatch, setNewsData, toggleFavorite } from '@@context/NewsDataContext'
import styles from './styles'

const NewsListScreen = (props) => {
  const [isLoading, setLoading] = useState(true)
  const [hasError, setHasError] = useState(false)
  const dispatch = useNewsDispatch()
  const { newsData } = useNewsState()

  const openNewsDetails = (id) => {
    props.navigation.navigate(SCREEN_NAME_NEWSDETAILS, { id })
  }

  const toggleFavoriteItem = (id) => {
    toggleFavorite(dispatch, id)
  }

  useEffect(() => {
    fetchNews()
      .then(res => setNewsData(dispatch, res.articles))
      .catch((error) => setHasError(error.message))
      .finally(() => { setLoading(false) })
  }, [])

  if (isLoading) {
    return (
      <View style={styles.newsListContainer}>
        <ActivityIndicator size='large' color='#0000ff' style={styles.indicator} />
      </View>
    )
  }

  if (hasError) {
    return (
      <View style={styles.newsListContainer}>
        <Text style={styles.error}>Somethig went wrong: {hasError}</Text>
      </View>
    )
  }

  return (
    <>
      <StatusBar barStyle='light-content' />
      <View style={styles.newsListContainer}>
        <FlatList
          keyExtractor={(item) => item.id.toString()}
          data={newsData}
          renderItem={({ item }) => <NewsItem newsItem={item} openNewsDetails={openNewsDetails} toggleFavoriteItem={toggleFavoriteItem} />}
        />
      </View>
    </>
  )
}

const NewsItem = (props) => {
  const { newsItem, openNewsDetails, toggleFavoriteItem } = props
  const { id, source: { name: sourceName }, author, title, urlToImage, publishedAt, isFavorite } = newsItem

  return (
    <View style={styles.newsItemContainer}>
      <TouchableOpacity style={styles.newsInfoContainer} onPress={() => openNewsDetails(id)}>
        <Image source={{ uri: urlToImage }} style={styles.newsItemImage} />
        <View style={styles.newsItemColumn}>
          <Text style={styles.newsItemTitle} numberOfLines={2}>{title}</Text>
          <Text style={styles.newsItemSubDetails}>{author || sourceName} • {getTimePassedSince(publishedAt)}</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={styles.newsItemFavCol} onPress={() => toggleFavoriteItem(id)}>
        <Text style={[styles.newsItemFavIcon, isFavorite ? styles.newsItemFavIconHighlight : {}]}>{isFavorite ? '★' : '☆'}</Text>
      </TouchableOpacity>
    </View>
  )
}

export default NewsListScreen
