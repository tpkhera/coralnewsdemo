import {
  StyleSheet
} from 'react-native'

const styles = StyleSheet.create({
  newsListContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  newsItemContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingStart: 16,
    paddingEnd: 16,
    marginTop: 8,
    marginBottom: 8
  },
  newsInfoContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  newsItemImage: {
    height: 75,
    width: 75,
    marginRight: 16,
    borderRadius: 8
  },
  newsItemColumn: {
    flex: 1,
    justifyContent: 'space-between',
    marginRight: 8
  },
  newsItemSubDetails: {
    fontSize: 12,
    color: '#666'
  },
  newsItemFavCol: {
    justifyContent: 'center'
  },
  newsItemFavIcon: {
    fontSize: 20,
    marginLeft: 16,
    color: '#666'
  },
  newsItemFavIconHighlight: {
    color: '#ffcc00'
  },
  indicator: {
    alignSelf: 'center'
  },
  error: {
    alignSelf: 'center',
    fontFamily: 'serif',
    marginLeft: 16,
    marginRight: 16
  }
})

export default styles
