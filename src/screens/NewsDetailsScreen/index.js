import React from 'react'
import {
  View,
  Text,
  Image,
  Linking,
  TouchableOpacity
} from 'react-native'

import { getTimePassedSince } from '@@helpers/utility-helper'
import { useNewsState, useNewsDispatch, toggleFavorite } from '@@context/NewsDataContext'
import styles from './styles'

const NewsDetailsScreen = (props) => {
  const { id } = props.route.params
  const dispatch = useNewsDispatch()
  const { newsData } = useNewsState()

  const newsItem = newsData.filter(item => item.id === id)[0]
  const { source: { name: sourceName }, author, title, description, url, urlToImage, publishedAt, isFavorite } = newsItem

  const toggleFavoriteItem = (id) => {
    toggleFavorite(dispatch, id)
  }

  const openNewsLink = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url)
      } else {
        console.log("Don't know how to open URI: " + this.props.url)
      }
    })
  }

  return (
    <View style={styles.newsDetailsContainer}>
      <Image source={{ uri: urlToImage }} style={styles.newsDetailsImage} />
      <Text style={styles.newsDetailsTitle}>{title}</Text>
      <View style={styles.newsDetailsHeading}>
        <View>
          <Text style={styles.newsItemSubDetails}>{author || sourceName} • {getTimePassedSince(publishedAt)}</Text>
        </View>
        <TouchableOpacity style={styles.newsItemFavCol} onPress={() => toggleFavoriteItem(id)}>
          <Text style={[styles.newsItemFavIcon, isFavorite ? styles.newsItemFavIconHighlight : {}]}>{isFavorite ? '★' : '☆'}</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.newsDetailsDescription}>{description}</Text>
      <TouchableOpacity style={styles.newsDetailsUrl} onPress={() => openNewsLink(url)}>
        <Text style={styles.newsDetailsUrlText}>Read more..</Text>
      </TouchableOpacity>
    </View>
  )
}

export default NewsDetailsScreen
