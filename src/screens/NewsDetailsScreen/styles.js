import {
  StyleSheet,
  Dimensions
} from 'react-native'

const styles = StyleSheet.create({
  newsDetailsContainer: {
    justifyContent: 'center',
    marginBottom: 16
  },
  newsDetailsImage: {
    width: Dimensions.get('window').width,
    height: 300
  },
  newsDetailsHeading: {
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 8
  },
  newsItemFavCol: {
    justifyContent: 'center'
  },
  newsItemFavIcon: {
    fontSize: 20,
    marginLeft: 16,
    color: '#666'
  },
  newsItemFavIconHighlight: {
    color: '#ffcc00'
  },
  newsItemSubDetails: {
    fontSize: 12,
    color: '#666'
  },
  newsDetailsTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    margin: 8
  },
  newsDetailsDescription: {
    marginLeft: 8,
    marginRight: 8,
    paddingTop: 24,
    fontFamily: 'serif'
  },
  newsDetailsUrl: {
    marginTop: 24,
    marginLeft: 8
  },
  newsDetailsUrlText: {
    color: '#0095ff',
    fontWeight: '500'
  }
})

export default styles
