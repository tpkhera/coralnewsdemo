import 'react-native-gesture-handler'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import codepush from 'react-native-code-push'

import { NewsProvider } from '@@context/NewsDataContext'
import { NewsListScreen, NewsDetailsScreen } from '@@screens'
import { SCREEN_NAME_NEWSLIST, SCREEN_NAME_NEWSDETAILS } from '@@helpers/constants-helper'

const { Navigator, Screen } = createStackNavigator()

const App = (props) => {
  return (
    <NavigationContainer>
      <NewsProvider>
        <Navigator>
          <Screen
            name={SCREEN_NAME_NEWSLIST}
            component={NewsListScreen}
            options={{ title: 'Latest News' }}
          />
          <Screen
            name={SCREEN_NAME_NEWSDETAILS}
            component={NewsDetailsScreen}
            options={{ title: 'News Details' }}
          />
        </Navigator>
      </NewsProvider>
    </NavigationContainer>
  )
}

const codepushApp = codepush(App)

export default codepushApp
