import * as timeago from 'timeago.js'

const getTimePassedSince = (date) => {
  return timeago.format(date)
}

export {
  getTimePassedSince
}
