// ideally, API keys should be in a separate .env file which is never committed to Git repo.
// including it here only for ease of project use.
const NEWS_API_KEY = 'dce927f230e0486e8cc6d82d08e247b3'
const NEWS_API_URL = `https://newsapi.org/v2/top-headlines?country=us&apiKey=${NEWS_API_KEY}`

const SCREEN_NAME_NEWSLIST = 'NewsList'
const SCREEN_NAME_NEWSDETAILS = 'NewsDetails'

export {
  NEWS_API_URL,
  SCREEN_NAME_NEWSLIST,
  SCREEN_NAME_NEWSDETAILS
}
