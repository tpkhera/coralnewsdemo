import { NEWS_API_URL } from './constants-helper'

const fetchNews = async () => {
  const res = await fetch(NEWS_API_URL)
  const resJson = await res.json()
  if (res.status !== 200) {
    throw new Error(resJson.message)
  }
  return resJson
}

export {
  fetchNews
}
