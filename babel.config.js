module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        root: ['./src'],
        alias: {
          '@@context': './src/context',
          '@@helpers': './src/helpers',
          '@@screens': './src/screens'
        }
      }
    ]
  ]
}
